const {
    createDirectory,
    createRandomFiles,
    deleteRandomFiles,
  } = require("../problem1.cjs");
  
  let directory = "dir";
  function testFunction() {
    try {
      //create directory and create random json files and delete random json files
      createDirectory(directory)
        .then(() => {
          let promisesArr = [];
          for (let i = 0; i < 3; i++) {
            let files = `./${directory}/file-${i}.json`;
            promisesArr.push(
              createRandomFiles(files, "data").then(() =>
                deleteRandomFiles(files)
              )
            );
          }
          return Promise.all(promisesArr);
        })
        .then(() => {
          console.log("All files created and deleted ");
        })
        .catch((err) => {
          console.log(err);
        });
    } catch (error) {
      console.log(error);
    }
  }
  
  testFunction();
  
  