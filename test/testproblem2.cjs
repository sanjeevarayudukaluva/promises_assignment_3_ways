const { readData, writeData, appendData, deleteData } = require("../problem2.cjs");

function testFunction() {
  try {
    //read data from "lipsum.txt" and convert it to uppercase
    readData("../lipsum.txt")
      .then((res) => {
        let upperCase = res.toUpperCase(res);

        return writeData("./upperCase.txt", upperCase);
      })
      .then(() => {
        return readData("./upperCase.txt");
      })
      .then((res) => {
        // Append "upperCase.txt" to the list of filenames in "filenames.txt"
        appendData("./filenames.txt", "upperCase.txt,");
        //read data from "uppercase.txt"
        return readData("./upperCase.txt");
      })
      .then((res) => {
        //converting data to lowercase and split it into sentences
        let convertLowerCase = res.toLowerCase();
        let splitingSentences = convertLowerCase.split(". ");
        //convert the split data into json format
        let splitingData = JSON.stringify(splitingSentences);

        return writeData("./spilting.txt", splitingData);
      })
      .then(() => {
        return readData("./spilting.txt");
      })

      .then((res) => {
        //append "splitting.txt" to the list of filenames in "filenames.txt"
        appendData("./filenames.txt", "spilting.txt,");
        //read data "spliting.txt"
        return readData("./spilting.txt");
      })
      .then((res) => {
        let sorting = JSON.parse(res).sort();
        console.log(sorting);
        return writeData("./sorting.txt", sorting);
      })
      .then(() => {
        return readData("./sorting.txt");
      })
      .then((res) => {
        appendData("./filenames.txt", "sorting.txt,");
        return readData("./filenames.txt");
      })
      .then((filenames) => {
        //  // Split filenames list and delete each file concurrently
        let deleteFiles = filenames
          .split(",")
          .filter((file) => file.trim() !== "");
        return Promise.all(deleteFiles.map(deleteData));
      })
      .then(() => {
        console.log("successfully deleted all files");
      })
      .catch((error) => {
        console.log(error);
      });
  } catch (error) {
    console.log(error);
  }
}
testFunction();
