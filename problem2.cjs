/* Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/
const fs = require("fs").promises;

function readData(filePath) {
  try {
    return fs.readFile(filePath, "utf-8");
  } catch (error) {
    console.log(error);
  }
}
function writeData(filePath, data) {
  try {
    return fs.writeFile(filePath, data);
  } catch (error) {
    console.log(error);
  }
}
function appendData(filePath, appendData) {
  try {
    return fs.appendFile(filePath, appendData);
  } catch (error) {
    console.log(error);
  }
}
function deleteData(fileNames) {
  try {
    fs.unlink(fileNames);
  } catch (error) {
    console.log(error);
  }
}

module.exports = { readData, writeData, appendData, deleteData };

