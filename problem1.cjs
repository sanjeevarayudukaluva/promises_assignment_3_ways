/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
const fs = require("fs").promises;
function createDirectory(directoryPath) {
  try {
    return fs.mkdir(directoryPath);
  } catch (error) {
    console.error(error);
  }
}
function createRandomFiles(fileNames, data) {
  try {
    return fs.writeFile(fileNames, data);
  } catch (error) {
    console.error(error);
  }
}
function deleteRandomFiles(files) {
  try {
    return fs.unlink(files);
  } catch (error) {
    console.error(error);
  }
}

module.exports = { createDirectory, createRandomFiles, deleteRandomFiles };
